# Backup restore  
### Prepare  
 * OS Ubuntu 20.04 used.
 * Install docker 'https://docs.docker.com/engine/install/ubuntu/' and docker-compose 'https://docs.docker.com/compose/install/'.
 * Install python3 'https://docs.python-guide.org/starting/install3/linux/', pip 'https://www.educative.io/answers/installing-pip3-in-ubuntu', docker module 'pip install docker' and python_on_whales 'pip install python_on_whales'.
 * run 'chmod +x' on 'get-dump.py' and 'send-result.py'.
### Execute
 * Run get-dump with access_token - 'get-dump.py 8fxxxxxxxxxx'. Script will get problem JSON, decode it to het dump, launch postgres db container, restore the dump and export JSON with alive ssn's.
 * Run send-result with access_token - 'send-result.py 8fxxxxxxxxxx'. Scrpipt will post JSON with alive ssn's to solution endpoint.

#! /usr/bin/python3
import requests
import json
import sys

url = "https://hackattic.com/challenges/backup_restore/solve?"
params = {
  "access_token":sys.argv # pass token as a param
}
with open("backups/result.json") as f:
   data = json.load(f)

response = requests.request("POST",url, params=params, data=json.dumps(data))
# submit json to service
print(response)

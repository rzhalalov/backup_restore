#! /usr/bin/python3
import requests
import json
import base64
import codecs
import os
import gzip
import shutil
import docker
import sys

url = "https://hackattic.com/challenges/backup_restore/problem?"
params = {
  "access_token":sys.argv # pass token as a param
}
response = requests.request("GET", url, params=params)
resp = json.loads(response.text)

dump_file = open("dump.txt", "w")
dump_file.write(resp["dump"])
dump_file.close()
# decode dump
data = open("dump.txt", "rb").read()
decoded = base64.b64decode(data)
decode_file = open("dump.gz", "wb")
decode_file.write(decoded)
decode_file.close()
# delete dump.txt
os.remove("dump.txt")

# unzip dump
with gzip.open('dump.gz', 'rb') as f_in:
    with open('backups/dump.sql', 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)
os.remove("dump.gz")

# run postgres
from python_on_whales import docker
docker.compose.up(detach=True)
# restore dump
os.system("docker-compose ps")

os.system("docker exec --workdir='/home/backups/' postgres sh restore.sh")
# query and save query data to json
os.system("docker exec --workdir='/home/backups/' postgres sh export.sh")

